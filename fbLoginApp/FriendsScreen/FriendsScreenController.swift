import UIKit

class FriendsScreenController: UITableViewController {

    private var tableArray = [String] ()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.register(FriendCell.self, forCellReuseIdentifier: "FriendCell")
        
        getFriends()
    }
    
    private func getFriends() {
        
        let url = URL(string: "https://uinames.com/api/?amount=25")
        
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            
            guard error == nil else {
                print("returning error")
                return
            }

            guard let content = data else {
                print("not returning data")
                return
            }
            
            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [[String:Any]] else {
                print("Error")
                return
            }
            
            for element in 0..<json.count {
                var friend: String = ""
                if let name = json[element]["name"] as? String {
                    friend.append(contentsOf: name)
                }
                if let surname = json[element]["surname"] as? String {
                    friend.append(contentsOf: " \(surname)")
                }
                
                self.tableArray.append(friend)
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }
        
        task.resume()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendCell", for: indexPath)
        
        cell.textLabel?.text = self.tableArray[indexPath.row]
        
        return cell
    }
 
}
