import Foundation

extension UIImageView {
    
    func setRounded() {
        self.layer.cornerRadius = (self.frame.width / 2)
        self.layer.masksToBounds = true
    }
}


extension UIColor {
    
    static var fbBlue: UIColor {
        return UIColor(red: 0.2590583265, green: 0.4043411314, blue: 0.6993427873, alpha: 1);
    }
    
    static var fbDarkBlue: UIColor {
        return UIColor(red: 0.1827078462, green: 0.2774667144, blue: 0.4770817757, alpha: 1);
    }
    
}

extension UIButton {
    
    /// Sets the background color to use for the specified button state.
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        
        let minimumSize: CGSize = CGSize(width: 1.0, height: 1.0)
        
        UIGraphicsBeginImageContext(minimumSize)
        
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(origin: .zero, size: minimumSize))
        }
        
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.clipsToBounds = true
        self.setBackgroundImage(colorImage, for: forState)
    }
}
