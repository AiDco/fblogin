import UIKit

class LoginController: UIViewController, FBSDKLoginButtonDelegate {
        
    private let loginButton: FBSDKLoginButton = {
        let button = FBSDKLoginButton()
        button.readPermissions = ["email"]
        return button
    }()
    
    private let friendsButton = UIButton()
    
    private var image = UIImageView()
    private var name = UILabel()
    private var email = UILabel()
    private var id = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        loginButton.delegate = self
        
        if FBSDKAccessToken.current() != nil {
            fetchProfile()
        }
        
        configure()
    }
    
    private func configure() {
        self.view.backgroundColor = .lightGray
        
        image.frame         = CGRect(x: 0, y: 0, width: 96, height: 96)
        image.center        = CGPoint(x: self.view.center.x, y: self.view.center.y - 80)
        image.contentMode   = UIView.ContentMode.scaleAspectFill
        image.setRounded()
        
        name.frame          = CGRect(x: 0, y: 0, width: 300, height: 40)
        name.center         = self.view.center
        name.textColor      = .black
        name.textAlignment  = .center
        name.numberOfLines  = 1
        name.font           = .systemFont(ofSize: 22.0)
        
        email.frame         = CGRect(x: 0, y: 0, width: 300, height: 40)
        email.center        = CGPoint(x: self.view.center.x, y: self.view.center.y + 30)
        email.textColor     = .black
        email.textAlignment = .center
        email.numberOfLines = 1
        email.font          = .systemFont(ofSize: 14.0)
        
        id.frame            = CGRect(x: 0, y: 0, width: 300, height: 40)
        id.center           = CGPoint(x: self.view.center.x, y: self.view.center.y + 60)
        id.textColor        = .black
        id.textAlignment    = .center
        id.numberOfLines    = 1
        id.font             = .systemFont(ofSize: 14.0)
        
        loginButton.center = CGPoint(x: self.view.center.x, y: self.view.frame.maxY - 160)
        
        friendsButton.frame = CGRect(x: self.view.center.x - (loginButton.frame.width / 2),
                                     y: self.view.frame.maxY - ( 160 + loginButton.frame.height + 24),
                                 width: loginButton.frame.width,
                                height: loginButton.frame.height)
        friendsButton.setTitle("Show friends", for: .normal)
        friendsButton.setTitleColor(.white, for: .normal)
        friendsButton.backgroundColor = .fbBlue
        friendsButton.titleLabel?.font =  .systemFont(ofSize: 13.0)
        friendsButton.setBackgroundColor(color: .fbDarkBlue, forState: .highlighted)
        friendsButton.layer.cornerRadius = 3
        friendsButton.addTarget(self, action: #selector(tapButton(sender:)), for: .touchUpInside)

        view.addSubview(loginButton)
    }
    
    private func fetchProfile() {
        print("fetch Profile")
        
        let parameters = ["fields": "id, email, first_name, last_name, picture.type(large)"]
        
        FBSDKGraphRequest(graphPath: "me", parameters: parameters)?.start(completionHandler: { (connection, result, error) -> Void in
            if error != nil {
                print(error!)
                return
            }
            
            guard let data = result as? NSDictionary else {
                return
            }
            
            guard let email = data["email"] as? String,
                  let name = data["first_name"] as? String,
                  let lastName = data["last_name"] as? String,
                  let id = data["id"] as? String,
                  let picture = data["picture"] as? NSDictionary,
                  let subData = picture["data"] as? NSDictionary,
                  let urlString = subData["url"] as? String
            else {
                return
            }
            
            self.name.text = "\(name) \(lastName)"
            self.email.text = email
            let url = URL.init(string: urlString)
            let image = try? Data(contentsOf: url!)
            self.image.image = UIImage(data: image!)
            self.id.text = "ID: \(id)"
            
        })
         loginComplete()
    }
    
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if (error == nil){
            if result.isCancelled {
                return
            }
        fetchProfile()
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        toInitial()
    }
    
    private func toInitial() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        friendsButton.removeFromSuperview()
        name.removeFromSuperview()
        email.removeFromSuperview()
        image.removeFromSuperview()
        id.removeFromSuperview()
    }
    
    private func loginComplete() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = "Personal Info"
        
        view.addSubview(friendsButton)
        view.addSubview(name)
        view.addSubview(email)
        view.addSubview(image)
        view.addSubview(id)
    }
    
    @objc func tapButton(sender: UIButton) {
        let newViewController = FriendsScreenController()
        self.navigationController?.pushViewController(newViewController, animated: true)
    }

}

